// SPDX-FileCopyrightText: 2023 Roman Gilg <subdiff@gmail.com>
//
// SPDX-License-Identifier: GPL-2.0-or-later

import QtQuick 2.3

QtObject {
    property bool visible: true
    property string message: "This is an example message.\nUsing multiple lines"
    property string iconName: "kwin"
}
