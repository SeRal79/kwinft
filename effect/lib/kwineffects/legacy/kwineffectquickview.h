/*
SPDX-FileCopyrightText: 2019 David Edmundson <davidedmundson@kde.org>

SPDX-License-Identifier: GPL-2.0-or-later
*/
#pragma once

// This header is deprecated, but installed for backwards compatibility. Do not include it anymore.
// Instead directly include the header below.
#include <kwineffects/effect_quick_view.h>

// Below header includes and forward declarations are preserved for backwards compatibility.
#include <kwineffects.h>

#include <QRect>
#include <QUrl>

class QKeyEvent;
class QMouseEvent;
